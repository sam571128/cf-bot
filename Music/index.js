import Discord from 'discord.js';
import ytdl from 'ytdl-core';

const delay = ms => new Promise(resolve => setTimeout(resolve, ms))

class Music {
    constructor() {
        this.isPlaying = false;
        this.queue = {};
        this.connection = {};
        this.dispatcher = {};
    }

    async join(msg) {
        try{
            // Bot 加入語音頻道
            this.connection[msg.guild.id] = await msg.member.voice.channel.join();
            msg.channel.send(`已加入語音頻道`);
        }catch(err){
            msg.channel.send(`加入時發生問題，也許你並沒有在語音頻道內`);
        }
    }

    async addMusic(msg,musicURL){
         // 取得 YouTube 影片名稱
         const guildID = msg.guild.id;
         const self = this;
         const songInfo = await ytdl.getInfo(musicURL);
         console.log(musicURL);
         console.log(songInfo.videoDetails.title);
 
         // 將歌曲資訊加入隊列
         if (!self.queue[guildID]) {
             self.queue[guildID] = [];
         }
 
         self.queue[guildID].push({
             name: songInfo.videoDetails.title,
             url: songInfo.videoDetails.video_url
         });

         console.log(self.isPlaying);

        // 如果目前正在播放歌曲就加入隊列，反之則播放歌曲
        if (self.isPlaying) {
            msg.channel.send(`歌曲加入隊列：${songInfo.videoDetails.title}`);
        } else {
            msg.channel.send("即將播放音樂")
            self.isPlaying = true;
            self.playMusic(msg, guildID, self.queue[guildID][0]);
        } 
        console.log("ADDED Music");
    } 

    async play(msg, musicURL) {
        // 語音群的 ID
        const guildID = msg.guild.id;

        // 如果 Bot 還沒加入該語音群的語音頻道
        if (!this.connection[guildID]) {
            this.join(msg);
            await delay(1000);
        }

        this.addMusic(msg,musicURL);
    }

    playMusic(msg, guildID, musicInfo) {
        // 提示播放音樂
        msg.channel.send(`播放音樂：${musicInfo.name}`);

        // 播放音樂
        this.dispatcher[guildID] = this.connection[guildID].play(ytdl(musicInfo.url, { filter: 'audioonly' }));

        // 把音量降 50%，不然第一次容易被機器人的音量嚇到 QQ
        this.dispatcher[guildID].setVolume(0.5);

        // 移除 queue 中目前播放的歌曲
        this.queue[guildID].shift();

        // 歌曲播放結束時的事件
        const self = this;
        this.dispatcher[guildID].on('finish', () => {

            // 如果隊列中有歌曲
            if (self.queue[guildID].length > 0) {
                self.playMusic(msg, guildID, self.queue[guildID].shift());
            } else {
                self.isPlaying = false;
                msg.channel.send('目前沒有音樂了，請加入音樂 :D');
            }

        });
    }

    resume(msg) {

        if (this.dispatcher[msg.guild.id]) {
            msg.channel.send('恢復播放');

            // 恢復播放
            this.dispatcher[msg.guild.id].resume();
        }

    }

    pause(msg) {

        if (this.dispatcher[msg.guild.id]) {
            msg.channel.send('暫停播放');

            // 暫停播放
            this.dispatcher[msg.guild.id].pause();
        }

    }

    skip(msg) {

        if (this.dispatcher[msg.guild.id]) {
            msg.channel.send('跳過目前歌曲');

            // 跳過歌曲
            this.dispatcher[msg.guild.id].end();
        }

    }

    nowQueue(msg) {
        let embed = new Discord.MessageEmbed()
        .setTitle('目前隊列中的歌曲')
        .setColor('RANDOM');
        // 如果隊列中有歌曲就顯示
        if (this.queue[msg.guild.id] && this.queue[msg.guild.id].length > 0) {
            // 字串處理，將 Object 組成字串
            const queueString = this.queue[msg.guild.id].map((item, index) => `[${index+1}] ${item.name}`).join('\n');
            embed.addField(`歌曲`,queueString)
            msg.channel.send(embed);
        } else {
            msg.channel.send('目前隊列中沒有歌曲');
        }

    }

    leave(msg) {
        this.isPlaying = false;
        try{
            // 離開頻道
            this.isPlaying = false;
            this.queue = {};
            this.connection[msg.guild.id].disconnect();
            this.connection = {};      
            msg.channel.send('已離開');  
        }catch(err){
            msg.channel.send('離開時發生問題');
        }
    }
}
let music = new Music();

const execute = async function(msg, cmd , args){
    if(cmd=='play'||cmd=='add'){
        if(args.length==0){
            if(!music.isPlaying&&music.queue.length!=0&&music.queue.length!=undefined){
                console.log(music.queue.length);
                music.isPlaying = true;
                music.playMusic(msg, msg.guild.id, music.queue[msg.guild.id][0]);
            }else{
                msg.channel.send('目前沒有音樂，請加入音樂 :D');
            }
            return;
        }
        if(args.length < 1 || args.length > 1){
            msg.reply("用法錯誤！ 正確用法: %play {youtube URL}");
            return;
        }
        // 如果使用者在語音頻道中
        if (msg.member.voice.channel) {

            // 播放音樂
            music.play(msg,args[0]);
        } else {

            // 如果使用者不在任何一個語音頻道
            msg.reply('你必須先加入語音頻道');
        }
        return;
    }
    if(cmd=='join'){
        music.join(msg);
        return;
    }
    if(cmd=='resume'){
        music.resume(msg);
        return;
    }
    if(cmd=='pause'){
        music.pause(msg);
        return;
    }
    if(cmd=='skip'){
        music.skip(msg);
        return;
    }
    if(cmd=='queue'){
        music.nowQueue(msg);
        return;
    }
    if(cmd=='leave'){
        music.leave(msg);
        return;
    }
}

const musics = {
    execute,
}

export default musics;