import Discord from 'discord.js';
import commands from './Commands/index.js';

import musics from './Music/index.js';

const bot = new Discord.Client();
const token = "NzMxMDkyMDAzMDU1NzMwNzE5.XwhbkQ.T9Iume1xL1tKH4SMRdNkPlaB4gc";
const prefix = '%';



bot.on('ready', () =>{
    console.log('The bot is on!')
    bot.user.setPresence({
        status: 'online',
        activity: {
            name: '持續解題中 %help',
            type: 'PLAYING',
        }
    })
})

bot.on('message', async msg=>{
    let channel = msg.channel;
    console.log(`頻道:${channel.name}，${msg.author.tag}:${msg.content}`);
    if(!msg.author.bot){
       if(msg.content=='87') channel.send('87'); 
       }
    if(!msg.content.startsWith(prefix)) 
        return;
    
    const args = msg.content.slice(prefix.length).split(" ");
    const cmd_name = args.shift();
    if(cmd_name=="sendch"){
        if(args.length < 2){
            msg.reply("用法錯誤！");
            return;
        }
        bot.channels.cache.get(args.shift()).send(args.join(" "));
        return;
    }

    musics.execute(msg,cmd_name,args);

    if(!commands[cmd_name])
        return;
    const cmd = commands[cmd_name];

    try{
        cmd.execute(msg,args);
    } catch(err){
        console.log("指令發生錯誤");
    }
});

bot.login(token);