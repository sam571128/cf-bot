import Discord from 'discord.js';

async function reactionDelete (botMessage, playerMessage) {

    const filter = (reaction, user) => {
        return ['✅'].includes(reaction.emoji.name) && user.id === playerMessage.author.id;
    };

    botMessage.react('✅');

    botMessage.awaitReactions(filter, { max: 1})
    .then(collected => {
        const reaction = collected.first();

        if (reaction.emoji.name === '✅') {
            botMessage.delete();
        }
    })
    .catch(collected => {
        if (botMessage.deletable) botMessage.reactions.removeAll();
    });

};

const execute = async function(msg, args){
    let channel = msg.channel;
    
    if(args.length!=0){
        if(args[0]=='training'){
            const m = channel.send(new Discord.MessageEmbed()
                .setColor('#8B008B')
                .setTitle(`訓練相關指令`)
                .addField('訓練過程中看題單',`請輸入%training status,題單一次出現的時間會是20秒\n若使用者解出題目,題單也會自動更新`)
                .addField(`變更題目`,`請輸入%training change (題號)`)
                .addField(`重置題目(或者設定題目難度)`,`請輸入%training reset (題目下界) (題目上界) (題目標籤 用|隔開 例如: math|greedy, -math|-implementation)`)
                .addField(`停止訓練`,`請輸入%training end`)).then(sentmsg=>{reactionDelete(sentmsg,msg)});
            msg.delete();
            return;
        }
    }
    const embed = new Discord.MessageEmbed()
        .setColor('#00FF55')
        .setTitle('所有指令 (難度上下界都可忽略不打)')
        .addField('Codeforce查詢','指令使用： %cf (cf handle)')
        .addField('找CF的問題','指令使用： %problem (難度下界) (難度上界) (標籤 以|隔開 可在tag前打-表示不想抽到那個tag的題目）')
        .addField('找CF的題單','指令使用： %mashup (題目數量) (難度下界) (難度上界) ')
        .addField('連接CF', '指令使用： %cf_reg (cf handle)')
        .addField('取消連接CF', '指令使用： %cf_del')
        .addField('決鬥功能','指令使用： %duel\n目前時間預設為120分鐘，若想提前結束或發生問題，請打%duel end')
        .addField('訓練功能(會自動挑選你CF的rating+0~+400的題目)','指令使用： %training')
        .addField('找ATC的問題','指令使用： %ATCproblem [比賽類型] [字母, A-F等等] 例如：%ATCproblem abc F');
    channel.send('',embed).then(sentmsg=>{reactionDelete(sentmsg,msg);});
    msg.delete();
}
const help = {
    name: 'help',
    usage: 'help[tags]',
    args: false,
    execute,
}
export default help;