import Discord from 'discord.js';
import data from './Database/data.js';

async function reactionDelete (botMessage, playerMessage) {

    const filter = (reaction, user) => {
        return ['❌'].includes(reaction.emoji.name) && user.id === playerMessage.author.id;
    };

    botMessage.react('❌');

    botMessage.awaitReactions(filter, { max: 1})
    .then(collected => {
        const reaction = collected.first();

        if (reaction.emoji.name === '❌') {
            botMessage.delete();
        }
    })
    .catch(collected => {
        if (botMessage.deletable) botMessage.reactions.removeAll();
    });

};

const execute = async function(msg,args){
    let channel = msg.channel;

    let list = await data.get(`CHEFLIST`);


    let sortList = [];

    for(let i = 0;i < list.length;i++){
        let cnt = await data.get(`CHEF${list[i]}`);
        sortList.push({id: `${list[i]}`, value: cnt})
    }

    sortList.sort(function(a,b){return b.value-a.value});

    const embedFooter = `使用者: ${msg.author.tag}`;
    const embed = new Discord.MessageEmbed()
        .setColor('#00FF55')
        .setTitle(`被廚排行榜`)
        .setFooter(embedFooter, `${msg.author.displayAvatarURL({
            size: 4096,
            dynamic: true
        })}`);
    for(let i = 0;i < sortList.length;i++){
        if(i==0) embed.addField(`電神冠軍： （被廚了 ${sortList[i].value} 次）`,`${sortList[i].id}`);
        else embed.addField(`第${i+1}名：（被廚了 ${sortList[i].value} 次）`,`${sortList[i].id}`);
    }
    msg.channel.send('',{embed}).then(sentmsg=>{reactionDelete(sentmsg,msg)});
    msg.delete();

}


const chefRank = {
    name: 'chefRank',
    usage: '%chefRank [someone]',
    execute
}
export default chefRank;