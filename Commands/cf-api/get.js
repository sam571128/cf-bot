export const API = {
    user: 'http://codeforces.com/api/user.info',
    problem: 'http://codeforces.com/api/problemset.problems',
    submissions: 'http://codeforces.com/api/problemset.recentStatus',
    userStatus: 'http://codeforces.com/api/user.status',
    Xuan: 'http://autoac.nctu.me/cfhandle/handle.json',
}