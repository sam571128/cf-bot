import fetch from 'node-fetch';
import {API} from './get.js';

export const getUser = async function(handle){
    const url = new URL(API.user);
    url.searchParams.append('handles',handle);
    return fetch(url).then(res=>{return res.json()});
}

export const getXuan = async function(){
    const url = new URL(API.Xuan);
    return fetch(url).then(res=>{return res.json()});
}

export const getSubmission = async function(count){
    const url = new URL(API.submissions);
    url.searchParams.append('count',count);
    return fetch(url).then(res=>{return res.json()});
}

export const getUserSubmission = async function(handle,count){
    const url = new URL(API.userStatus);
    url.searchParams.append('handle',handle);
    if(count!=='all'){
        url.searchParams.append('from',1);
        url.searchParams.append('count', count);
    }
    return fetch(url).then(res=>{return res.json()});
}

export const getProblem = async function(tags) {
    let params = tags.join(';');
    const url = new URL(API.problem);
    url.searchParams.append('tags',params);
    return fetch(url).then(res=>{return res.json();});
};
