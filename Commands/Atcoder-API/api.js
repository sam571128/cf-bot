import fetch from 'node-fetch';
import {API} from './get.js';

export const getProblem = async function() {
    const url = new URL(API.problem);
    return fetch(url).then(res=>{return res.json();});
};

