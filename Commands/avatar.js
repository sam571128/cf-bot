import Discord from 'discord.js';

async function reactionDelete (botMessage, playerMessage) {

    const filter = (reaction, user) => {
        return ['❌'].includes(reaction.emoji.name) && user.id === playerMessage.author.id;
    };

    botMessage.react('❌');

    botMessage.awaitReactions(filter, { max: 1})
    .then(collected => {
        const reaction = collected.first();

        if (reaction.emoji.name === '❌') {
            botMessage.delete();
        }
    })
    .catch(collected => {
        if (botMessage.deletable) botMessage.reactions.removeAll();
    });

};

const execute = async function(msg,args){
    let channel = msg.channel;

    let target = msg.mentions.users.first();

    if (!target)
        target = msg.author;

    let avatarURL = target.displayAvatarURL({
        size: 4096,
        dynamic: true
    });

    const embedTitle = `頭貼`;
    const embedFooter = `使用者: ${msg.author.tag}`;

    const Embed = new Discord.MessageEmbed()
        .setTitle(embedTitle)
        .setDescription(`<@${target.id}>的頭貼`)
        .setDescription(`[圖片連結](${avatarURL})`)
        .setColor('RANDOM')
        .setImage(avatarURL)
        .setFooter(embedFooter, `${msg.author.displayAvatarURL({
            size: 4096,
            dynamic: true
        })}`);
    msg.channel.send('',Embed).then(sentmsg=>{reactionDelete(sentmsg,msg)});
    msg.delete();
}


const avatar = {
    name: 'avatar',
    usage: '%avatar [someone]',
    execute
}
export default avatar;