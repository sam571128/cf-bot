import help from './help.js';
import cf_reg from './cf_reg.js';
import cf_del from './cf_del.js';
import problem from './problem.js';
import duel from './duel.js';
import del from './del.js';
import connectXuan from './connectXuan.js';
import updateUsersProblems from './updateUsersProblems.js';
import mashup from './mashup.js';
import training from './training.js';
import sent from './sent.js';
import avatar from './avatar.js';
import cf from './cf.js';
import force_handle from './force_handle.js';
import ATCproblem from './ATCproblem.js';
import chef from './chef.js';
import chefrank from './chefRank.js';
import csa_mashup from './csa_mashup.js';

const commands = {
    help,
    cf_reg,
    problem,
    cf_del,
    duel,
    del,
    connectXuan,
    updateUsersProblems,
    mashup,
    training,
    sent,
    avatar,
    cf,
    force_handle,
    ATCproblem,
    chef,
    chefrank,
    csa_mashup
}

export default commands;