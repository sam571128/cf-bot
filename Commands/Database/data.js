import Keyv from '@keyv/mongo';
const keyv = new Keyv('mongodb://heroku_2s0x54xn:j4dp7vhacj5i9ep88ugetuod4s@cluster-cf-bot-shard-00-00.vls3u.mongodb.net:27017,cluster-cf-bot-shard-00-01.vls3u.mongodb.net:27017,cluster-cf-bot-shard-00-02.vls3u.mongodb.net:27017/heroku_2s0x54xn?ssl=true&replicaSet=atlas-8elzf1-shard-0&authSource=admin&retryWrites=true&w=majority');


keyv.on('error', err=> console.error('Keyv connection error:', err));
const save = async function(username,handle){
    keyv.set(username,handle);
}
const del = async function(username){
    await keyv.delete(username);
}
const get = async function(username){
    let result = await keyv.get(username).then(res=>{return res});
    return result;
}

const data = {
    save,
    get,
    del,
}
export default data;