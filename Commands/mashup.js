import Discord from 'discord.js';
import { getProblem, getUser } from './cf-api/api.js';
import data from './Database/data.js';

const execute = async function(msg,args){
    
    let min_rtg = 0, max_rtg = 4000, amount, stat = false;
    let cfHandle = await data.get(`<@${msg.author.id}>`);
    if(cfHandle == undefined) stat = false;
    else{
        let USER = await getUser(cfHandle);
        stat = true;
        min_rtg = Math.floor(USER.result[0].rating/100)*100;
        max_rtg = USER.result[0].rating + 200;
    }
    if(args.length > 4|| args.length == 0){
        msg.reply('指令用法錯誤。正確用法：%mashup (題目數量) (題目難度下界) (題目難度上界)').then(sentmsg=>{sentmsg.delete({timeout:5000})});
        msg.delete();
        return;
    }
    let user_tags = [''];
    let no_tags = [];
    if(args.length > 0){
        let str = "";
        while(isNaN(args[args.length-1])){
            str = args[args.length-1] + " " + str;
            args.pop();
            if(args.length == 0) break;
        }
        user_tags = str.split('|');
    }
    for(let i = 0;i < user_tags.length;i++){
        
        if(user_tags[i].startsWith('-')){
            no_tags.push(user_tags[i].substring(1,user_tags[i].length));
            user_tags.splice(i,1);
            if(user_tags.length==0) user_tags = [''];
            i--;
        }
    }
    
    if(!isNaN(args[args.length-1])){
        if(args.length > 1){
            min_rtg = args[args.length-2];
            max_rtg = args[args.length-1];
            if(args.length==3) amount = args[args.length-3];
        }else if(args.length==1){
            amount = args[args.length-1];
        }
    }else{
        msg.reply('指令用法錯誤。正確用法：%mashup (題目數量) (題目難度下界) (題目難度上界)').then(sentmsg=>{sentmsg.delete({timeout:5000})});
        msg.delete();
        return;
    }
    let result;
    try{
        const body = await getProblem(user_tags);
        result = body.result;
    }catch(err){
        if(err.status && err.status === 400){
            console.log("現在CF發生問題，請稍後再試");
            return;
        }
    }
    let problems;
    try{
        problems = result.problems;
    }catch(err){
        msg.reply('現在CF發生問題，請稍後再試');
    }
    if(!problems.length){
        msg.reply('找不到你所想要找的問題');
        return;
    }

    if(!stat){
        let probs = [];
        for(let i = 0;i < amount;i++){
            let r = Math.round((Math.random()*(problems.length-1)));
            let count = 0;
            while(problems[r].contestId < 300 ||problems[r].tags.includes('*special')||problems[r].rating < min_rtg || problems[r].rating > max_rtg || problems[r].rating == null){
                count++;
                r = Math.round((Math.random()*(problems.length-1)));
                for(let i = 0;i < no_tags.length;i++){
                    if(problems[r].tags.includes(no_tags[i])){
                        check = false;
                    }
                }
                if(count > 10000){
                    msg.channel.send('發生錯誤');
                    return;
                }
            }
            probs.push(r);
        }
        probs.sort((a,b)=>problems[a].rating-problems[b].rating);
        let embed = new Discord.MessageEmbed()
            .setTitle('為你精心設計的題單')
            .setColor('#FFFF00');
        let str = '';
        for(let i = 0;i < probs.length;i++){
            str += `[${problems[probs[i]].name}](http://codeforces.com/contest/${problems[probs[i]].contestId}/problem/${problems[probs[i]].index})`;
            if(i!=probs.length-1) str += '\n';
        }
        embed.addField('題目',str);
        str = '';
        for(let i = 0;i < probs.length;i++){
            str+=`||${problems[probs[i]].rating}||\t\t||${problems[probs[i]].tags.join(', ')}||`;
            if(i!=probs.length-1) str += '\n';
        }
        embed.addField('難度\t\t標籤',str);
        const embedFooter = `使用者: ${msg.author.tag}`;
        embed.setFooter(embedFooter, `${msg.author.displayAvatarURL({
            size: 4096,
            dynamic: true
        })}`);
        msg.channel.send('',embed);
        msg.delete();
    }else{
        let sub = await data.get(`SUB${cfHandle}`);
        if(sub==undefined) sub = [];
        let probs = [];
        for(let i = 0;i < amount;i++){
            let r = Math.round((Math.random()*(problems.length-1)));
            let count = 0;
            while(probs.includes(r)||problems[r].contestId < 300 ||problems[r].tags.length==0||problems[r].tags.includes('*special')||sub.includes(problems[r].contestId+problems[r].index)||problems[r].rating < min_rtg || problems[r].rating > max_rtg || problems[r].rating == null){
                count++;
                r = Math.round((Math.random()*(problems.length-1)));
                if(count > 10000){
                    msg.channel.send('發生錯誤，可能是難度選擇錯誤或者找不到你沒寫過的題目');
                    return;
                }
            }
            probs.push(r);
        }
        probs.sort((a,b)=>problems[a].rating-problems[b].rating);
        let embed = new Discord.MessageEmbed()
            .setTitle('為你精心設計的題單')
            .setColor('#FFFF00');
        let str = '';
        for(let i = 0;i < probs.length;i++){
            str += `[${problems[probs[i]].name}](http://codeforces.com/contest/${problems[probs[i]].contestId}/problem/${problems[probs[i]].index})`;
            if(i!=probs.length-1) str += '\n';
        }
        embed.addField('題目',str);
        str = '';
        for(let i = 0;i < probs.length;i++){
            str+=`||${problems[probs[i]].rating}||\t\t||${problems[probs[i]].tags.join(', ')}||`;
            if(i!=probs.length-1) str += '\n';
        }
        embed.addField('難度\t\t標籤',str);
        const embedFooter = `使用者: ${msg.author.tag}`;
        embed.setFooter(embedFooter, `${msg.author.displayAvatarURL({
            size: 4096,
            dynamic: true
        })}`);
        msg.channel.send('',embed);
        msg.delete();
    }
}

const mashup = {
    name: 'mashup',
    usage: '%mashup [problems amount]',
    execute,
}
export default mashup; 