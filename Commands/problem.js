import Discord from 'discord.js';
import {getProblem} from './cf-api/api.js';
import fetch from 'node-fetch';
import {API} from './cf-api/get.js';
import data from './Database/data.js';

const execute = async function(msg, args){
    let min_rtg = 0;
    let max_rtg = 4000;
    let user_tags = [''];
    let userhandle = await data.get(`<@${msg.author.id}>`);
    let usersub = await data.get(`SUB${userhandle}`);
    if(usersub==undefined){
        usersub = [];
    }
    let no_tags = [];
    if(args.length > 0){
        let str = "";
        while(isNaN(args[args.length-1])){
            str = args[args.length-1] + " " + str;
            args.pop();
            if(args.length == 0) break;
        }
        user_tags = str.split('|');
        if(args.length > 0){
            if(!isNaN(args[args.length-1])&&!isNaN(args[args.length-2])){
                min_rtg = parseInt(args[args.length-1]); args.pop();
                max_rtg = parseInt(args[args.length-1]); args.pop();
                if(min_rtg > max_rtg){
                    let tmp = max_rtg;
                    max_rtg = min_rtg;
                    min_rtg = tmp;
                }
            }
        }
    }
    for(let i = 0;i < user_tags.length;i++){
        
        if(user_tags[i].startsWith('-')){
            no_tags.push(user_tags[i].substring(1,user_tags[i].length-1));
            user_tags.splice(i,1);
            if(user_tags.length==0) user_tags = [''];
            i--;
        }
    }
    let result;
    try{
        const body = await getProblem(user_tags);
        result = body.result;
    }catch(err){
        if(err.status && err.status === 400){
            console.log("現在CF發生問題，請稍後再試");
            return;
        }
    }
    let problems;
    try{
        problems = result.problems;
    }catch(err){
        msg.reply('現在CF發生問題，請稍後再試');
    }
    if(!problems.length){
        msg.reply('找不到你所想要找的問題');
        return;
    }
    let r = Math.round((Math.random()*(problems.length-1)));
    let count = 0;
    let check = true;
    for(let i = 0;i < no_tags.length;i++){
        console.log(no_tags[i]);
        if(problems[r].tags.includes(no_tags[i])){
            check = false;
        }
    }
    while(problems[r].contestId < 300 ||!check||problems[r].tags.length==0||problems[r].tags.includes('*special')||usersub.includes(problems[r].contestId+problems[r].index) || problems[r].rating < min_rtg || problems[r].rating > max_rtg || problems[r].rating == null){
        check = true;
        count++;
        r = Math.round((Math.random()*(problems.length-1)));
        for(let i = 0;i < no_tags.length;i++){
            if(problems[r].tags.includes(no_tags[i])){
                console.log(no_tags[i]);
                check = false;
            }
        }
        if(count > 30000){
            msg.reply('找不到你所想要找的問題，可能是難度、標籤選擇錯誤或者找不到你沒寫過的題目');
            return;
        }
    }
    const embedFooter = `使用者: ${msg.author.tag}`;
    const {name, contestId, index, tags, rating} = problems[r];
    const embed = new Discord.MessageEmbed()
        .setColor('#00FF55')
        .setThumbnail('https://sta.codeforces.com/s/76530/images/codeforces-telegram-square.png')
        .setTitle(name)
        .addField('來自哪一場比賽', `${contestId}${index}`)
        .addField('標籤', `||${tags.join(', ')}||`)
        .addField('難度',`||${rating}||`)
        .setURL(`http://codeforces.com/contest/${contestId}/problem/${index}`)
        .setFooter(embedFooter, `${msg.author.displayAvatarURL({
            size: 4096,
            dynamic: true
        })}`);
    msg.channel.send('',{embed});
    msg.delete();
}

const problem = {
    name: 'problem',
    usage: 'problem[tags]',
    args: false,
    execute,
}
export default problem;