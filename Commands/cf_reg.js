import Discord from 'discord.js'
import data from './Database/data.js';
import {getProblem, getSubmission, getUserSubmission, getUser} from './cf-api/api.js';

const execute = async function(msg, args){
    if(args.length==0||args.length > 1){
        msg.reply('指令用法錯誤。正確用法：%cf_reg (cf handle)');
        return;
    }
    
    let user = '<@' + msg.author.id + '>';
    let cf_handle = args[0];
    const check = await getUser(cf_handle);
    if(check.status === 'FAILED'){
        msg.reply('使用者名稱不存在');
        return;
    }
    let db = await data.get(user);
    if(db!==undefined){
        msg.reply('你已經連接一個cf handle了');
        return;
    }
    const empty_tag = [''];
    const getproblem = await getProblem(empty_tag);
    let result = getproblem.result;
    const {problems} = result;
    let r = Math.round(Math.random()*(problems.length-1));
    const {contestId,index} = problems[r];
    const embed = new Discord.MessageEmbed()
        .setColor('#0055FF')
        .setTitle('CF註冊')
        .addField('註冊','請在一分鐘內對此題傳送CE')
        .addField('題目',`http://codeforces.com/contest/${contestId}/problem/${index}`)
    msg.channel.send('',embed);
    setTimeout(savedata,60000,msg,user,cf_handle,problems[r]);
}

const savedata = async function(msg,user,cf_handle,p){
    try{
        const sub = await getUserSubmission(cf_handle,1);
        const {result} = sub;
        let {problem} = result[0];
        const {contestId,index} = problem;
        if(result[0].verdict === 'COMPILATION_ERROR' && p.contestId===contestId && p.index === index){
            const status = await getUser(cf_handle);
            const embed = new Discord.MessageEmbed()
                .setTitle('註冊成功')
                .setThumbnail(status.result[0].titlePhoto)
                .setColor('#00FF55')
                .addField('CF使用者名稱', cf_handle)
                .addField('Rating', status.result[0].rating)
                .addField('Rank', status.result[0].rank);
            msg.channel.send('',embed);
            let users = await data.get('USER');
            if(!users.includes(cf_handle)) users.push(cf_handle);
            data.save(user,cf_handle);
            data.save('USER',users);
        }else{
            const embed = new Discord.MessageEmbed()
                .setColor('#FF2222')
                .setTitle('註冊失敗')
            msg.channel.send('',embed);
        }
    }catch(err){
        const embed = new Discord.MessageEmbed()
            .setColor('#FF2222')
            .setTitle('註冊失敗，與CF連線有問題')
        msg.channel.send('',embed);
    }
}

const cf_reg = {
    name: 'cf_reg',
    usage: 'cf_reg [cf_handle]',
    execute,
}

export default cf_reg;