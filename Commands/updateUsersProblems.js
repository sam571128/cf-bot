import Discord from 'discord.js';
import data from './Database/data.js';
import { getUserSubmission } from './cf-api/api.js';
const execute = async function(msg,args){
    let d = await data.get('USER');
    if(d==undefined){
        msg.reply('查無資料，請稍後再試');
        return;
    }
    console.log(d);
    let arr = await data.get('PROB');
    if(arr==undefined){
        data.save('PROB',[]);
        arr = [];
    }
    for(let i = 0;i < d.length;i++){
        await new Promise(r => setTimeout(r,1000));
        let tmp = await getUserSubmission(`${d[i]}`,'all');
        let sub = await data.get(`SUB${d[i]}`);
        if(sub==undefined){
            data.save(`SUB${d[i]}`,[]);
            sub = [];
        }
        //console.log(tmp);
        try{
            let result = tmp.result;
            //console.log(result);
            for(let j = 0;j < result.length;j++){
                if(result[j].verdict==='OK'){
                    if(!sub.includes(`${result[j].problem.contestId}${result[j].problem.index}`)){
                        sub.push(`${result[j].problem.contestId}${result[j].problem.index}`);
                    }
                    if(!arr.includes(`${result[j].problem.contestId}${result[j].problem.index}`)){
                        arr.push(`${result[j].problem.contestId}${result[j].problem.index}`);
                    }
                }
            }
            console.log(sub);
            data.save(`SUB${d[i]}`,sub);
        }catch(e){
            continue;
        }
    }
    data.save('PROB',[]);
    msg.reply('更新完成').then(sentmsg=>{sentmsg.delete({timeout:1000})});
    msg.delete();
}

const updateUsersProblems = {
    name : 'updateUsersProblems',
    execute,
}
export default updateUsersProblems;