import Discord from 'discord.js'
import data from './Database/data.js';
import {getProblem, getSubmission, getUserSubmission, getUser} from './cf-api/api.js';

const execute = async function(msg, args){
    let user = args[0].split('!').join('');
    data.del(user);
    let cf_handle = args[1];
    const status = await getUser(cf_handle);
    const embed = new Discord.MessageEmbed()
        .setTitle('註冊成功')
        .setThumbnail(status.result[0].titlePhoto)
        .setColor('#00FF55')
        .addField('CF使用者名稱', cf_handle)
        .addField('Rating', status.result[0].rating)
        .addField('Rank', status.result[0].rank);
    msg.channel.send('',embed);
    let users = await data.get('USER');
    if(!users.includes(cf_handle)) users.push(cf_handle);
    data.save(user,cf_handle);
    data.save('USER',users);
}

const force_handle = {
    name: 'force_handle',
    usage: 'force_handle [cf_handle]',
    execute,
}

export default force_handle;