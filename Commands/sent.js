import Discord from 'discord.js';

const execute = function(msg,args){
    let channel = msg.channel;
    let sendmsg = "";
    let times = 1;
    for(let i = 0;i < args.length;i++){
        let tmp = args[i].split('=');
        if(tmp[0]=='msg') sendmsg = tmp[1];
        else if(tmp[0]=='times') times = tmp[1];
    }
    console.log(sendmsg,times);
    for(let i = 0;i < times;i++){
        channel.send(sendmsg);
    }
}

const sent = {
    name: 'sent',
    execute
}

export default sent;