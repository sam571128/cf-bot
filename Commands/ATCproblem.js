import Discord from 'discord.js';
import {getProblem} from './Atcoder-API/api.js';

const execute = async function(msg, args){

    let contestType = "NO", Letter = "NO";

    if(args.length >= 1){
        contestType = args[0].toLowerCase();
    }

    if(args.length >= 2){
        Letter = args[1].toUpperCase();
    }

    let problems = await getProblem();

    let r = Math.round((Math.random()*(problems.length-1))), cnt = 0;
    
    while((contestType!="NO"&&!problems[r].contest_id.includes(contestType))||(Letter!="NO"&&problems[r].title.charAt(0)!=Letter)||new RegExp(/[\u3000-\u303f\u3040-\u309f\u30a0-\u30ff\uff00-\uff9f\u4e00-\u9faf\u3400-\u4dbf]/).test(problems[r].title)){
        r = Math.round((Math.random()*(problems.length-1)));
        if(cnt>=10000){
            msg.channel.send("找不到所指定的題目");
            msg.delete();
            return;
        }
        
    }

    const embedFooter = `使用者: ${msg.author.tag}`;
    const {title, contest_id, id} = problems[r];
    const embed = new Discord.MessageEmbed()
        .setColor('#00FF55')
        .setThumbnail('https://img.atcoder.jp/assets/atcoder.png')
        .setTitle(title)
        .addField('來自哪一場比賽', `${contest_id}`)
        .setURL(`https://atcoder.jp/contests/${contest_id}/tasks/${id}`)
        .setFooter(embedFooter, `${msg.author.displayAvatarURL({
            size: 4096,
            dynamic: true
        })}`);
    msg.channel.send('',{embed});
    msg.delete();
}

const ATCproblem = {
    name: 'problem',
    usage: 'problem[tags]',
    args: false,
    execute,
}
export default ATCproblem;