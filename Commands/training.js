import Discord from 'discord.js';
import data from './Database/data.js';
import { getProblem, getUserSubmission, getUser } from './cf-api/api.js';

async function reactionDelete (botMessage, playerMessage) {

    const filter = (reaction, user) => {
        return ['✅'].includes(reaction.emoji.name) && user.id === playerMessage.author.id;
    };

    botMessage.react('✅');

    botMessage.awaitReactions(filter, { max: 1})
    .then(collected => {
        const reaction = collected.first();

        if (reaction.emoji.name === '✅') {
            botMessage.delete();
        }
    })
    .catch(collected => {
        if (botMessage.deletable) botMessage.reactions.removeAll();
    });

};

const execute = async function(msg,args){
    let user = `<@${msg.author.id}>`;
    let channel = msg.channel;
    let cfHandle = await data.get(`${user}`);
    let userProblems = await data.get(`TRAIN${user}`);
    let Training = await data.get(`TRAINING${user}`);
    let tmp = await getProblem(['']);
    let problems = tmp.result.problems;
    if(Training){
        if(args[0]=='end'||args[0]=='stop'){
            data.save(`TRAINING${user}`,false);
            channel.send('已經幫你結束訓練').then(sentmsg=>{sentmsg.delete({timeout:3000})});
            msg.delete();
            return;
        }else if(args[0]=='status'){   
            let userEXP = await data.get(`TRAINEXP${user}`);
            let userProbs = await data.get(`TRAINP${user}`);
            let nowTime = await data.get(`TRAINT${user}`);
            let maxtime = 3600;
            let hour = Math.floor((maxtime-nowTime)/3600);
            let minute = Math.floor((maxtime-nowTime)%3600/60);
            let second = (maxtime-nowTime)%60;
            if(userEXP==undefined||userEXP!=userEXP) userEXP = 0;
            if(userProbs==undefined) userProbs = 0;
            const embed = new Discord.MessageEmbed()
                .setTitle(`${cfHandle}的訓練單`)
                .setColor('#FFFF00')
                .setFooter(`已累計題數：${userProbs}\t\t累計經驗值${userEXP}\t\t剩餘時間:${hour}小時${minute}分鐘${second}秒`);
            let str = '';
            for(let i = 0;i < userProblems.length;i++){
                if(problems[userProblems[i]]==undefined) continue;
                str += `[${problems[userProblems[i]].name}](http://codeforces.com/contest/${problems[userProblems[i]].contestId}/problem/${problems[userProblems[i]].index})`;
                if(i!=userProblems.length-1) str += '\n';
            }
            embed.addField('題目',str);
            str = '';
            for(let i = 0;i < userProblems.length;i++){
                if(problems[userProblems[i]]==undefined) continue;
                str+=`||${problems[userProblems[i]].rating}||\t\t||${problems[userProblems[i]].tags.join(', ')}||`;
                if(i!=userProblems.length-1) str += '\n';
            }
            embed.addField('難度\t\t標籤',str);
            channel.send('',embed).then(sentmsg=>{reactionDelete(sentmsg,msg)});
            msg.delete();
            return;
        }else if(args[0]=='change'){
            if(args.length==1){
                channel.send('指令用法錯誤，要換題目請輸入%training change (第幾題)').then(sentmsg=>{sentmsg.delete({timeout: 5000})});
                msg.delete();
                return;
            }else{
                let p = args[1]-1;
                let r = Math.round((Math.random()*(problems.length-1)));
                let count = 0;
                let sub = await data.get(`SUB${cfHandle}`);
                while(problems[r].contestId < 300 ||problems[r].tags.length==0||problems[r].tags.includes('*special')||sub.includes(problems[r].contestId+problems[r].index)||problems[r].rating < userProblems[p].rating || problems[r].rating > userProblems[p].rating || problems[r].rating == null){
                    count++;
                    r = Math.round((Math.random()*(problems.length-1)));
                    if(count > 50000){
                        channel.send(new Discord.MessageEmbed().setTitle('發生錯誤')).then(sentmsg=>{sentmsg.delete({timeout:5000})});
                        return;
                    }
                }
                userProblems[p] = r;
                data.save(`TRAIN${user}`,userProblems);
                channel.send(new Discord.MessageEmbed().setTitle('已經幫你把題目換掉，請打%training status開啟題單')).then(sentmsg=>{sentmsg.delete({timeout:10000})});
                msg.delete();
                return;
            }
        }else if(args[0]=='reset'){
            let USER = await getUser(cfHandle);
            let min_rtg = Math.max(800,Math.floor(USER.result[0].rating/100)*100-200);
            let max_rtg = Math.max(1200,USER.result[0].rating + 200);
            let usertags = [];
            let no_tags = [];
            if(args.length!=1&&isNaN(args[args.length-1])){
                console.log(args[args.length-1]);
                usertags = args[args.length-1].split('|');
                args.pop();
            }
            try{
                if(usertags.length!=0){
                    for(let i = 0;i < usertags.length;i++){
                        if(usertags[i].startsWith('-')){
                            no_tags.push(usertags[i].substring(1,usertags[i].length));   
                            usertags.splice(i,1);
                            if(usertags.length==0) usertags = [''];
                            i--;
                        }
                    }
                    let tmp = await getProblem(usertags);
                    if(tmp.result.problems.length!=0){
                        problems = tmp.result.problems;
                        data.save(`TRAIN${user}`,problems);
                    }
                }
            }catch(err){}
            if(args.length==3){
                console.log(args);
                min_rtg = args[1];
                max_rtg = args[2];
            }
            console.log(min_rtg,max_rtg);
            if(parseInt(min_rtg)+400 > parseInt(max_rtg)){
                max_rtg = parseInt(min_rtg)+400;
                channel.send(`題目難度間隔太小已將難度上界調為${max_rtg}`).then(sentmsg=>{sentmsg.delete({timeout:5000})});
            } 
            let sub = await data.get(`SUB${cfHandle}`);
            if(sub==undefined) sub = [];
            console.log(sub);
            for(let i = 0;i < 5;i++){
                let r = Math.round((Math.random()*(problems.length-1)));
                let count = 0;
                let check = false;
                while(problems[r].rating==undefined||problems[r].contestId < 300 ||!check||problems[r].tags.length==0||problems[r].tags.includes('*special')||sub.includes(problems[r].contestId+problems[r].index) || problems[r].rating < parseInt(parseInt(min_rtg)+parseInt(i*(max_rtg-min_rtg)/5))|| problems[r].rating > parseInt(parseInt(min_rtg)+parseInt((i+1)*(max_rtg-min_rtg)/5)) ||　problems[r].rating == null){
                    count++;
                    check = true;
                    for(let i = 0;i < no_tags.length;i++){
                        if(problems[r].tags.includes(no_tags[i])){
                            check = false;
                        }
                    }
                    r = Math.round(Math.random()*(problems.length-1));
                    if(count > 50000){
                        channel.send(`發生錯誤，找不到適合${user}的題目，可能出現難度不正確的題目`);
                        break;
                    }
                }
                userProblems[i]=r;
            }
            channel.send(new Discord.MessageEmbed().setTitle('已經幫你把題目換掉，請打%training status開啟題單')).then(sentmsg=>{sentmsg.delete({timeout:10000})});
            data.save(`TRAIN${user}`,userProblems);
            msg.delete();
            return;
        }else{
            channel.send('你已經在進行訓練了，若發生問題，請輸入%training end').then(sentmsg=>{sentmsg.delete({timeout:5000})});
            msg.delete();
            return
        }
    }
    
    if(cfHandle==undefined){
        msg.reply('你尚未註冊CF帳號，請使用%cf (cf handle)註冊').then(sentmsg=>{sentmsg.delete({timeout: 5000})});
        msg.delete();
        return;
    }
    if(userProblems==undefined){
        userProblems = [];
        data.save(`TRAIN${user}`,userProblems);
    }
    let USER = await getUser(cfHandle);
    if(userProblems.length==0){
        let min_rtg = Math.max(800,Math.floor(USER.result[0].rating/100)*100-200);
        let max_rtg = Math.max(1200,USER.result[0].rating + 200);
        let sub = await data.get(`SUB${cfHandle}`);
        if(sub==undefined) sub = [];
        for(let i = 0;i < 5;i++){
            let r = Math.round((Math.random()*(problems.length-1)));
            let count = 0;
            while(problems[r].rating==undefined||problems[r].contestId < 300 || problems[r].tags.length==0||problems[r].tags.includes('*special')||sub.includes(problems[r].contestId+problems[r].index)||problems[r].rating < min_rtg || problems[r].rating > max_rtg || problems[r].rating == null){
                count++;
                r = Math.round((Math.random()*(problems.length-1)));
                if(count > 50000){
                    channel.send(new Discord.MessageEmbed().setTitle('發生錯誤')).then(sentmsg=>{sentmsg.delete({timeout:5000})});
                    return;
                }
            }
            userProblems.push(r);
        }
        userProblems.sort((a,b)=>problems[a].rating-problems[b].rating);
        data.save(`TRAIN${user}`,userProblems);
    }
    channel.send(`${user}`).then(sentmsg=>{sentmsg.delete({timeout:5000})});
    channel.send(new Discord.MessageEmbed()
        .setColor('#8B008B')
        .setTitle(`訓練即將開始(時間：一小時)\n`)
        .addField(`提示：`,'若是第一次用此功能的用戶，預設題目難度會是你CF rating -200~+200\n若有改變難度的需求，請用第三個指令')
        .addField('訓練過程中看題單',`請輸入%training status,題單一次出現的時間會是60秒\n若使用者解出題目,題單也會自動更新`)
        .addField(`變更題目(自動挑選同樣難度)`,`請輸入%training change (題號)`)
        .addField(`重置題目(或者設定題目難度)`,`請輸入%training reset (題目下界) (題目上界) (題目標籤 用|隔開 例如: math|greedy, -math|-implementation)`)
        .addField(`停止訓練`,`請輸入%training end`)).then(sentmsg=>{reactionDelete(sentmsg,msg)});
    channel.send(new Discord.MessageEmbed()
        .setColor('#823452')
        .setTitle(`查詢訓練相關指令，請輸入%help training`)
    )
    data.save(`TRAINWA${user}`,[]);
    data.save(`TRAINGETEXP${user}`,0);
    data.save(`TRAINGPROBS${user}`,0);
    data.save(`TRAINING${user}`,true);
    await new Promise(r => setTimeout(r,2000));
    checkStatus(msg,`${user}`,problems,cfHandle,USER.result[0].rating,true);
    let timeStart = setInterval(timer,1000,`${user}`);
    let train = setInterval(checkStatus,5000,msg,`${user}`,problems,cfHandle,USER.result[0].rating,false);
    msg.delete();
    data.save(`TRAINT${user}`,0);
    let nowTime = await data.get(`TRAINT${user}`);
    let isTraining = await data.get(`TRAINING${user}`);
    while(isTraining&&nowTime<3600){
        await new Promise(r => setTimeout(r,2000));
        nowTime = await data.get(`TRAINT${user}`);
        isTraining = await data.get(`TRAINING${user}`);
    }
    clearInterval(train);
    clearInterval(timeStart);
    console.log('Stopped');
    let getEXP = await data.get(`TRAINGETEXP${user}`);
    let getProbs = await data.get(`TRAINGPROBS${user}`);
    let embed = new Discord.MessageEmbed()
    if(isTraining){
        embed.setColor('#FF0000')
            .setTitle('時間到，若想繼續訓練，請重新打%training指令')
            .addField('訓練者',user)
            .addField('本次獲得經驗值',getEXP)
            .addField('本次解的題數',getProbs);
    }
    else{
        embed.setColor('#FF0000')
            .setTitle(`訓練強制結束，若想繼續訓練，請重新打%training指令`)
            .addField('訓練者',user)
            .addField('本次獲得經驗值',getEXP)
            .addField('本次解的題數',getProbs);
    }
    channel.send(embed);
    data.save(`TRAINING${user}`,false);
    data.del(`TRAINWA${user}`);
    data.save(`TRAINT${user}`,0);
    data.save(`TRAINGETEXP${user}`,0);
    data.save(`TRAINGPROBS${user}`,0)
}

const checkStatus = async function(msg,user,problems,cfHandle,rating,first){
    let channel = msg.channel;
    let UserSubmissions = await getUserSubmission(cfHandle,5);
    let UserResult = await UserSubmissions.result;
    let userProblems = await data.get(`TRAIN${user}`);
    let UserWA = await data.get(`TRAINWA${user}`);
    let UserGetEXP = await data.get(`TRAINGETEXP${user}`);
    let usersub = await data.get(`SUB${cfHandle}`);
    let UserSOLVED = await data.get(`TRAINGPROBS${user}`);
    if(UserResult==undefined){
        channel.send('目前無法連接上Codeforces').then(sentmsg=>{sentmsg.delete({timeout:5000})});
        return;
    }
    if(usersub==undefined){
        usersub = [];
    }
    let ok = false;
    for(let i = 0;i < UserResult.length;i++){
        for(let j = 0;j < userProblems.length;j++){
            if(UserResult[i].problem.contestId==problems[userProblems[j]].contestId&&UserResult[i].problem.index==problems[userProblems[j]].index){
                if(UserResult[i].verdict === 'OK'){
                    ok = true;
                    channel.send(`${user}已解出${problems[userProblems[j]].name}，難度為${problems[userProblems[j]].rating}，並獲得${Math.ceil(1/(1+Math.exp(-(problems[userProblems[j]].rating-rating)/100))*300)}點經驗值`).then(sentmsg=>{sentmsg.delete({timeout:30000})});
                    let userEXP = await data.get(`TRAINEXP${user}`);
                    let userProbs = await data.get(`TRAINP${user}`);
                    if(userEXP==undefined||userEXP!=userEXP) userEXP = 0;
                    if(userProbs==undefined) userProbs = 0;
                    UserGetEXP += Math.ceil(1/(1+Math.exp(-(problems[userProblems[j]].rating-rating)/100))*300);
                    UserSOLVED += 1;
                    data.save(`TRAINEXP${user}`,userEXP+Math.ceil(1/(1+Math.exp(-(problems[userProblems[j]].rating-rating)/100))*300));
                    data.save(`TRAINP${user}`,userProbs+1);
                    data.save(`TRAINGETEXP${user}`,UserGetEXP);
                    data.save(`TRAINGPROBS${user}`,UserSOLVED);
                    let r = Math.round((Math.random()*(problems.length-1)));
                    let count = 0;
                    while(problems[r].rating==undefined||problems[r].contestId < 300 ||problems[r].tags.length==0||problems[r].tags.includes('*special')||usersub.includes(problems[r].contestId+problems[r].index) || problems[r].rating < problems[userProblems[j]].rating || problems[r].rating > problems[userProblems[j]].rating || problems[r].rating == null){
                        count++;
                        r = Math.round((Math.random()*(problems.length-1)));
                        if(count > 30000){
                            msg.reply('找不到同樣難度的題目').then(sentmsg=>{sentmsg.delete({timeout:5000})});
                            break;
                        }
                    }
                    userProblems[j] = r;
                    data.save(`TRAIN${user}`,userProblems); 
                }else if(!UserWA.includes(UserResult[i].id)&&UserResult[i].verdict!=='TESTING'){
                    channel.send(`${user}解了${problems[userProblems[j]].name}，但是答案錯了，請再接再厲`).then(sentmsg=>{sentmsg.delete({timeout:30000})});
                    UserWA.push(UserResult[i].id);
                    data.save(`TRAINWA${user}`,UserWA);
                }
            }
        }
    }
    if(ok||first){
        let userEXP = await data.get(`TRAINEXP${user}`);
        userEXP = Math.max(0,userEXP);
        data.save(`TRAINEXP${user}`,userEXP);
        let userProbs = await data.get(`TRAINP${user}`);
        let nowTime = await data.get(`TRAINT${user}`);
        let maxtime = 3600;
        let hour = Math.floor((maxtime-nowTime)/3600);
        let minute = Math.floor((maxtime-nowTime)%3600/60);
        let second = (maxtime-nowTime)%60;
        if(userEXP==undefined||userEXP!=userEXP) userEXP = 0;
        if(userProbs==undefined) userProbs = 0;
        const embed = new Discord.MessageEmbed()
            .setTitle(`${cfHandle}的訓練單`)
            .setColor('#FFFF00')
            .setFooter(`已累計題數：${userProbs}\t\t累計經驗值${userEXP}\t\t剩餘時間:${hour}小時${minute}分鐘${second}秒`);
        let str = '';
        for(let i = 0;i < userProblems.length;i++){
            str += `[${problems[userProblems[i]].name}](http://codeforces.com/contest/${problems[userProblems[i]].contestId}/problem/${problems[userProblems[i]].index})`;
            if(i!=userProblems.length-1) str += '\n';
        }
        embed.addField('題目',str);
        str = '';
        for(let i = 0;i < userProblems.length;i++){
            str+=`||${problems[userProblems[i]].rating}||\t\t||${problems[userProblems[i]].tags.join(', ')}||`;
            if(i!=userProblems.length-1) str += '\n';
        }
        embed.addField('難度\t\t標籤',str);
        channel.send('',embed).then(sentmsg=>{sentmsg.delete({timeout:60000})});
    }
}

const timer = async function(user){
    let tick = await data.get(`TRAINT${user}`);
    data.save(`TRAINT${user}`,tick+1);
}

const training = {
    name: 'training',
    execute,
}

export default training;