import Discord from 'discord.js';
import data from './Database/data.js';
import { getProblem, getUserSubmission, getUser } from './cf-api/api.js';

async function reactionDelete (botMessage, playerMessage) {

    const filter = (reaction, user) => {
        return ['✅'].includes(reaction.emoji.name) && user.id === playerMessage.author.id;
    };

    botMessage.react('✅');

    botMessage.awaitReactions(filter, { max: 1})
    .then(collected => {
        const reaction = collected.first();

        if (reaction.emoji.name === '✅') {
            botMessage.delete();
        }
    })
    .catch(collected => {
        if (botMessage.deletable) botMessage.reactions.removeAll();
    });

};

const execute = async function(msg,args){
    if(args.length==0){
        let user = `<@${msg.author.id}>`;
        let cfHandle = await data.get(`${user}`);
        if(cfHandle==undefined){
            msg.reply("你尚未註冊，請輸入%cf_reg (cf handle)進行註冊，若要查詢某個人的cf資料，請輸入%cf (cf handle)");
            return;
        }
        const status = await getUser(cfHandle);
        if(status.status=="FAILED"){
            msg.reply("現在可能無法連上Codeforces或者是沒有此handle的資料");
            return;
        }
        var rating = '無' , maxRating = '無' , rank = '無' , maxRank = '無' , city = '無' , organization = '無';
        try {rating = status.result[0].rating;} catch(err){ rating = '無';}
        try {maxRating = status.result[0].maxRating;} catch(err){ maxRating = '無';}
        try {rank = status.result[0].rank;} catch(err){rank = '無';}
        try {maxRank = status.result[0].maxRank;} catch(err){ maxRank = '無';}
        try {city = status.result[0].city; }catch(err){ city = '無';}
        try {organization = status.result[0].organization;} catch(err) {organization = '無';}
        if(rating==undefined||rating==''||rating==' ') rating = '無';
        if(maxRating==undefined||maxRating==''||maxRating==' ') maxRating = '無';
        if(rank==undefined||rank==''||rank==' ') rank = '無';
        if(maxRank==undefined||maxRank==''||maxRank==' ') maxRank = '無';
        if(city==undefined||city==''||city==' ') city = '無';
        if(organization==undefined||organization==''||organization==' ') organization = '無';

        console.log(rating, maxRating, rank, maxRank, city, organization);
        const embed = new Discord.MessageEmbed()
            .setTitle('Codeforces查詢')
            .setThumbnail(status.result[0].titlePhoto)
            .setColor('#FF0000')
            .addField('使用者名稱', cfHandle)
            .addField('目前分數', rating)
            .addField('歷史最高分數',maxRating)
            .addField('牌位', rank)
            .addField('歷史最高牌位', maxRank)
            .addField('居住城市',city)
            .addField('來自的學校或組織', organization)
            .setFooter(`使用者: ${msg.author.tag}`, `${msg.author.displayAvatarURL({
                size: 4096,
                dynamic: true
            })}`);
        msg.channel.send('',embed).then(sentmsg=>{reactionDelete(sentmsg,msg)});
    }else{
        let cfHandle = args[0];
        const status = await getUser(cfHandle);
        if(status.status=="FAILED"){
            msg.reply("現在可能無法連上Codeforces或者是沒有此handle的資料");
            return;
        }
        var rating = '無' , maxRating = '無' , rank = '無' , maxRank = '無' , city = '無' , organization = '無';
        try {rating = status.result[0].rating;} catch(err){ rating = '無';}
        try {maxRating = status.result[0].maxRating;} catch(err){ maxRating = '無';}
        try {rank = status.result[0].rank;} catch(err){rank = '無';}
        try {maxRank = status.result[0].maxRank;} catch(err){ maxRank = '無';}
        try {city = status.result[0].city; }catch(err){ city = '無';}
        try {organization = status.result[0].organization;} catch(err) {organization = '無';}
        if(rating==undefined||rating==''||rating==' ') rating = '無';
        if(maxRating==undefined||maxRating==''||maxRating==' ') maxRating = '無';
        if(rank==undefined||rank==''||rank==' ') rank = '無';
        if(maxRank==undefined||maxRank==''||maxRank==' ') maxRank = '無';
        if(city==undefined||city==''||city==' ') city = '無';
        if(organization==undefined||organization==''||organization==' ') organization = '無';

        console.log(rating, maxRating, rank, maxRank, city, organization);
        const embed = new Discord.MessageEmbed()
            .setTitle('Codeforces查詢')
            .setThumbnail(status.result[0].titlePhoto)
            .setColor('#FF0000')
            .addField('使用者名稱', cfHandle)
            .addField('目前分數', rating)
            .addField('歷史最高分數',maxRating)
            .addField('牌位', rank)
            .addField('歷史最高牌位', maxRank)
            .addField('居住城市',city)
            .addField('來自的學校或組織', organization)
            .setFooter(`使用者: ${msg.author.tag}`, `${msg.author.displayAvatarURL({
                size: 4096,
                dynamic: true
            })}`);
        msg.channel.send('',embed).then(sentmsg=>{reactionDelete(sentmsg,msg)});
    }
    msg.delete();
}

const cf = {
    name: 'cf',
    usage: '%cf {tag people}',
    execute,
}
export default cf;