import Discord from 'discord.js';
import data from './Database/data.js';
import { getProblem, getUserSubmission, getUser } from './cf-api/api.js';

async function reactionDelete (botMessage, playerMessage) {

    const filter = (reaction, user) => {
        return ['✅'].includes(reaction.emoji.name) && user.id === playerMessage.author.id;
    };

    botMessage.react('✅');

    botMessage.awaitReactions(filter, { max: 1})
    .then(collected => {
        const reaction = collected.first();

        if (reaction.emoji.name === '✅') {
            botMessage.delete();
        }
    })
    .catch(collected => {
        if (botMessage.deletable) botMessage.reactions.removeAll();
    });

};

const execute = async function(msg, args){
    let isBattling = await data.get(`B<@${msg.author.id}>`);
    let gettingRequest = await data.get(`A<@${msg.author.id}>`); 
    let choosingMode = await data.get(`MODE<@${msg.author.id}>`);

    if(choosingMode!=undefined){
        if(isNaN(args[0])||args[0]==0||args[0]>2){
            msg.reply('指令用法錯誤, 選擇模式的指令使用為: %duel (第幾個模式)').then(sentmsg=>{sentmsg.delete({timeout:5000})});
            return;
        }
        data.del(`MODE<@${msg.author.id}>`);  
        let playerB = choosingMode; 
        if(args[0]==1){
            let min_rtg = 0, max_rtg = 4000, maxtime = 7200;
            let embed = new Discord.MessageEmbed()
                .setTitle(`決鬥設定`)
                .setColor('#FFFF00')
                .addField(`模式`,`第一種`)
                .addField('發起決鬥的人',`<@${msg.author.id}>`);
            if(args.length==1){
                embed.addField('題目下界',min_rtg,true);
                embed.addField('題目上界',max_rtg,true);
                let hour = Math.floor((maxtime)/3600);
                let minute = Math.floor((maxtime)%3600/60);
                let second = (maxtime)%60;
                embed.addField('時間',`${hour}小時${minute}分鐘${second}秒`);
                msg.channel.send(embed).then(sentmsg=>{sentmsg.delete({timeout:60000})});
                msg.channel.send(`${playerB}，請在1分鐘內輸入%duel accept接受邀請或使用%duel reject拒絕邀請`).then(sentmsg=>{sentmsg.delete({timeout:60000})});
                data.save(`A${playerB}`,[`<@${msg.author.id}>`,`${min_rtg}`,`${max_rtg}`,`${maxtime}`,`${args[0]}`]);
                data.save(`C<@${msg.author.id}>`,true);
                setTimeout(CoolDown, 60000, msg);
                setTimeout(StopWaiting,60000,msg,`A${playerB}`);
                msg.delete();
                return;
            }else if(args.length==2){
                maxtime = args[1];
                embed.addField('題目下界',min_rtg,true);
                embed.addField('題目上界',max_rtg,true);
                let hour = Math.floor((maxtime)/3600);
                let minute = Math.floor((maxtime)%3600/60);
                let second = (maxtime)%60;
                embed.addField('時間',`${hour}小時${minute}分鐘${second}秒`);
                msg.channel.send(embed).then(sentmsg=>{sentmsg.delete({timeout:60000})});
                msg.channel.send(`${playerB}，請在1分鐘內輸入%duel accept接受邀請或使用%duel reject拒絕邀請`).then(sentmsg=>{sentmsg.delete({timeout:60000})});
                data.save(`A${playerB}`,[`<@${msg.author.id}>`,`${min_rtg}`,`${max_rtg}`,`${maxtime}`,`${args[0]}`]);
                data.save(`C<@${msg.author.id}>`,true);
                setTimeout(CoolDown, 60000, msg);
                setTimeout(StopWaiting,60000,msg,`A${playerB}`);
                msg.delete();
                return;
            }else if(args.length == 3){
                min_rtg = args[1], max_rtg = args[2];
                embed.addField('題目下界',min_rtg,true);
                embed.addField('題目上界',max_rtg,true);
                let hour = Math.floor((maxtime)/3600);
                let minute = Math.floor((maxtime)%3600/60);
                let second = (maxtime)%60;
                embed.addField('時間',`${hour}小時${minute}分鐘${second}秒`);
                msg.channel.send(embed).then(sentmsg=>{sentmsg.delete({timeout:60000})});
                msg.channel.send(`${playerB}，請在1分鐘內輸入%duel accept接受邀請或使用%duel reject拒絕邀請`).then(sentmsg=>{sentmsg.delete({timeout:60000})});
                data.save(`A${playerB}`,[`<@${msg.author.id}>`,`${min_rtg}`,`${max_rtg}`,`${maxtime}`,`${args[0]}`]);
                data.save(`C<@${msg.author.id}>`,true);
                setTimeout(CoolDown, 60000, msg);
                setTimeout(StopWaiting,60000,msg,`A${playerB}`);
                msg.delete();
                return;
            }else if(args.length == 4){
                min_rtg = args[1], max_rtg = args[2], maxtime = args[3];
                embed.addField('題目下界',min_rtg,true);
                embed.addField('題目上界',max_rtg,true);
                let hour = Math.floor((maxtime)/3600);
                let minute = Math.floor((maxtime)%3600/60);
                let second = (maxtime)%60;
                embed.addField('時間',`${hour}小時${minute}分鐘${second}秒`);
                msg.channel.send(embed).then(sentmsg=>{sentmsg.delete({timeout:60000})});
                msg.channel.send(`${playerB}，請在1分鐘內輸入%duel accept接受邀請或使用%duel reject拒絕邀請`).then(sentmsg=>{sentmsg.delete({timeout:60000})});
                data.save(`A${playerB}`,[`<@${msg.author.id}>`,`${min_rtg}`,`${max_rtg}`,`${maxtime}`,`${args[0]}`]);
                data.save(`C<@${msg.author.id}>`,true);
                setTimeout(CoolDown, 60000, msg);
                setTimeout(StopWaiting,60000,msg,`A${playerB}`);
                msg.delete();
                return;
            }else{
                msg.reply('指令用法錯誤, 選擇第一個模式的指令使用為: %duel 1 (難度下界) (難度上界) (決鬥長度)').then(sentmsg=>{sentmsg.delete({timeout:5000})});
                msg.delete();
                return;
            }
        }
        if(args[0]==2){
            /*let min_rtg = 0, max_rtg = 4000, maxtime = 7200;
            if(args.length!=0||args.length<3){
                if(args.length==2) maxtime = args[1];
                msg.channel.send(`${playerB}，請在1分鐘內輸入%duel accept接受邀請或使用%duel reject拒絕邀請`).then(sentmsg=>{sentmsg.delete({timeout:60000})});
                data.save(`A${playerB}`,[`<@${msg.author.id}>`,`${min_rtg}`,`${max_rtg}`,`${maxtime}`,`${args[0]}`]);
                data.save(`C<@${msg.author.id}>`,true);
                setTimeout(CoolDown, 60000, msg);
                setTimeout(StopWaiting,60000,msg,`A${playerB}`);
                return;
            }else{
                msg.reply('指令用法錯誤, 選擇第一個模式的指令使用為: %duel 2 (決鬥長度)').then(sentmsg=>{sentmsg.delete({timeout:10000})});
                return;
            }*/
        }
    }
    //是否正在決鬥當中
    if(isBattling!=undefined){
        if(args[0]=='end'||args[0]=='stop'){
            data.del(`ARR<@${msg.author.id}>`);
            data.del(`ARR${isBattling[0]}`);
            data.del(`B<@${msg.author.id}>`);
            data.del(`B${isBattling[0]}`)
            data.save(`T${isBattling[0]}`,0);
            data.save(`T<@${msg.author.id}>`,0);
            /*data.del(`P<@${isBattling}>`); 
            data.del(`SCORE<@${isBattling}>`);
            data.del(`SCORE<@${isBattling}>`);*/
            msg.reply('已經幫你取消決鬥').then(sentmsg=>{sentmsg.delete({timeout:5000})});
            return;
        }
        if(args[0]=='status'){
            if(isBattling[1]==1){
                let playerA = isBattling[0];
                let playerB = `<@${msg.author.id}>`;
                let problems = await data.get(`P${playerA}`);
                let arr = await data.get(`ARR${playerA}`);
                let AHandle = await data.get(playerA);
                let BHandle = await data.get(playerB);
                let AScore = await data.get(`SCORE${playerA}`);
                let BScore = await data.get(`SCORE${playerB}`);
                

                let embed = new Discord.MessageEmbed()
                    .setTitle(`決鬥進行狀況 \n${AHandle}目前的分數${AScore}  vs  ${BScore}目前的分數${BHandle}`)
                    .setColor('#0055FF');
                let str = '';   
                for(let i = 0;i < arr.length;i++){
                    str += `[${problems[arr[i][0]].name}](http://codeforces.com/contest/${problems[arr[i][0]].contestId}/problem/${problems[arr[i][0]].index})`;
                    if(i!=arr.length-1) str += '\n';
                }
                embed.addField('題目',str);
                str = '';
                for(let i = 0;i < arr.length;i++){
                    str += `難度:${problems[arr[i][0]].rating}\t分數:${arr[i][1]}`;
                    if(i!=arr.length-1) str += '\n';
                }
                embed.addField('難度及分數',str);
                let nowTime = await data.get(`T${playerA}`);
                let maxtime = await data.get(`MAXT${playerA}`);
                let hour = Math.floor((maxtime-nowTime)/3600);
                let minute = Math.floor((maxtime-nowTime)%3600/60);
                let second = (maxtime-nowTime)%60;
                str = `${hour}小時${minute}分鐘${second}秒`;
                embed.addField('剩餘時間',str);
                msg.channel.send('',embed).then(sentmsg=>{reactionDelete(sentmsg,msg)});
                msg.delete();
                return;
            }else if(isBattling[1]==2){
                let playerA = isBattling[0];
                let playerB = `<@${msg.author.id}>`;
                let maxtime = await data.get(`MAXT${playerA}`);
                let problems = await data.get(`P${playerA}`);
                let arr1 = await data.get(`ARR${playerA}`);
                let arr2 = await data.get(`ARR${playerA}`);
                let AHandle = await data.get(playerA);
                let BHandle = await data.get(playerB);
                let AHP = await data.get(`HP${playerA}`);
                let BHP = await data.get(`HP${playerB}`);
                let embed = new Discord.MessageEmbed()
                    .setTitle(`決鬥進行狀況 \n${AHandle}目前的生命值${AHP}\t\t\t  vs  \t\t\t${BHP}目前的生命值${BHandle}`)
                    .setColor('#0055FF');
                let str = '';   
                for(let i = 0;i < arr1.length;i++){
                    str += `難度:${problems[arr1[i][0]].rating}\t攻擊力:${arr1[i][1]}\t\t[${problems[arr1[i][0]].name}](http://codeforces.com/contest/${problems[arr1[i][0]].contestId}/problem/${problems[arr1[i][0]].index})`;
                    if(i!=arr1.length-1) str += '\n';
                }
                embed.addField(`${AHandle}抽到的難度、攻擊力、題目`,str,true);
                str = '';   
                for(let i = 0;i < arr2.length;i++){
                    str += `難度:${problems[arr2[i][0]].rating}\t攻擊力:${arr2[i][1]}\t\t[${problems[arr2[i][0]].name}](http://codeforces.com/contest/${problems[arr2[i][0]].contestId}/problem/${problems[arr2[i][0]].index})`;
                    if(i!=arr2.length-1) str += '\n';
                }
                embed.addField(`${BHandle}抽到的難度、攻擊力、題目`,str,true);
                let nowTime = await data.get(`T${playerA}`);
                let hour = Math.floor((maxtime-nowTime)/3600);
                let minute = Math.floor((maxtime-nowTime)%3600/60);
                let second = (maxtime-nowTime)%60;
                str = `${hour}小時${minute}分鐘${second}秒`;
                embed.addField('剩餘時間',str);
                msg.channel.send('',embed).then(sentmsg=>{sentmsg.delete({timeout:5000})});
                msg.delete();
                return;
            }
        }
        msg.reply('你正在進行決鬥，若是發生問題，請輸入%duel end 或使用%duel status來查看目前的決鬥情況').then(sentmsg=>{sentmsg.delete({timeout:5000})});
        msg.delete();
        return;
    }
    //是否收到決鬥邀請
    if(gettingRequest!=undefined&&args[0]!='accept'&&args[0]!='reject'){
        msg.reply('有人邀請你進行決鬥，請先接受或拒絕後在繼續之後的動作').then(sentmsg=>{sentmsg.delete({timeout:5000})});
        msg.delete();
        return;
    }

    //拒絕決鬥邀請
    if(args[0]==='reject'){
        let tmp = await data.get(`A<@${msg.author.id}>`);
        data.del(`A<@${msg.author.id}>`);
        if(tmp==undefined){
            msg.reply('並未有人邀請你參加決鬥').then(sentmsg=>{sentmsg.delete({timeout:5000})});
            msg.delete();
            return;
        }
        msg.reply(`你已回絕${tmp[0]}的邀請`).then(sentmsg=>{sentmsg.delete({timeout:5000})});
        msg.delete();
        return;
    }

    //接受決鬥邀請，並呼叫開始決鬥
    if(args[0]==='accept'){
        console.log('有人正在決鬥！');
        let tmp = await data.get(`A<@${msg.author.id}>`);
        data.del(`A<@${msg.author.id}>`);
        if(tmp==undefined){
            msg.reply('並未有人邀請你參加決鬥').then(sentmsg=>{sentmsg.delete({timeout:5000})});
            msg.delete();
            return;
        }
        data.del(`STOP<@${msg.author.id}>`);
        data.del(`STOP<@${msg.author.id}>`);
        msg.channel.send(`${tmp[0]}和<@${msg.author.id}>的決鬥即將開始，請拭目以待。`).then(sentmsg=>{sentmsg.delete({timeout:10000})});
        data.save(`B<@${msg.author.id}>`,[`${tmp[0]}`,`${tmp[4]}`]);
        data.save(`B${tmp[0]}`,[`<@${msg.author.id}>`,`${tmp[4]}`]);
        if(tmp[4]==1) BattleStart(msg,tmp[0],`<@${msg.author.id}>`,tmp[1],tmp[2],tmp[3]);
        else if(tmp[4]==2) BattleStart2(msg,tmp[0],`<@${msg.author.id}>`,tmp[3]);
        return;
    }
    //發起決鬥的冷卻時間，避免BUG
    let cd = await data.get(`C<@${msg.author.id}>`);
    if(cd != undefined){
        msg.reply('你一分鐘前已發起過決鬥邀請，請一分鐘後再試一次').then(sentmsg=>{sentmsg.delete({timeout:5000})});
        msg.delete();
        return;
    }
    let min_rtg = 0, max_rtg = 4000, playerB, maxtime = 7200; 
    if(args.length == 0|| args.length > 1||!isNaN(args[0])||!args[0].startsWith('<@')){
        msg.reply('指令用法錯誤。正確用法：%duel (@discord用戶)').then(sentmsg=>{sentmsg.delete({timeout:10000})});
        msg.delete();
        return;
    }
    //若資料庫無發起者資料，請他去註冊
    let checkA = data.get('<@' + msg.author.id +'>')
    if(checkA==undefined){
        msg.reply('你尚未完成CF註冊，請使用!cf_reg (cf handle)').then(sentmsg=>{sentmsg.delete({timeout:10000})});
        msg.delete();
        return;
    }
    //決定玩家
    if(isNaN(args[args.length-1])){
        playerB = args[args.length-1].split('!').join('');
        args.pop();
    }
    /*if(playerB == `<@${msg.author.id}>`){
        msg.reply('自己打自己是不行的呦');
        return;
    }*/
    let check = await data.get(playerB);
    if(check == undefined){
        msg.channel.send(playerB+'尚未完成CF註冊').then(sentmsg=>{sentmsg.delete({timeout:5000})});
        msg.delete();
        return;
    }
    let hasRequest = await data.get(`A${playerB}`);
    if(hasRequest!=undefined){
        msg.channel.send(`${playerB}已經收到一個邀請了，請稍後再試`).then(sentmsg=>{sentmsg.delete({timeout:5000})});
        msg.delete();
        return;
    }
    msg.reply('請在一分鐘內完成模式的選擇').then(sentmsg=>{sentmsg.delete({timeout:60000})});
    const embed = new Discord.MessageEmbed()
        .setColor('#C87630')
        .setTitle('選擇模式 (請輸入%duel 1, %duel 2,以此類推) (在指令後面輸入數字 可以決定決鬥長度，例如：%duel 1 3600就是一小時)')
        .addField('模式1 (若選擇此模式 可用%duel 1 (難度下界) (難度上界) 來選擇難度範圍)','雙方共用5題，分數分別為100~500，第一個解決問題的人獲得分數，該題會從題目表上剃除\n當時間結束或5題皆被解完，決鬥結束，獲得分數最高者獲勝')
        .addField('模式2(已廢除)',"真正的決鬥，雙方會有各自的血量，也會各自得到5題，題目的難度會是你CF的rating-500~+500\n每次解完一題，將會再獲得下一題難度相等的題目，題目對敵人所造成的傷害預設為100~500\n當其中一方血量歸零，或者時間到，剩餘血量最多的玩家獲勝");
    msg.channel.send(embed).then(sentmsg=>{sentmsg.delete({timeout:60000})});
    data.save(`MODE<@${msg.author.id}>`,playerB);
    setTimeout(DelMode,60000,msg);
    msg.delete();
    
}

const DelMode = async function(msg){
    let tmp = await data.get(`MODE<@${msg.author.id}>`);
    if(tmp!=undefined){
        msg.reply('選擇模式失敗').then(sentmsg=>{sentmsg.delete({timeout:5000})});
        data.del(`MODE<@${msg.author.id}>`);
    }
    
}

const CoolDown = async function(msg){
    data.del(`C<@${msg.author.id}>`);
}

const StopWaiting = async function(msg,playerB){
    let user = await data.get(playerB);
    if(user!=undefined){
        msg.reply('發起決鬥失敗'); 
        data.del(playerB);
    } 
}

const BattleStart2 = async function(msg,playerA,playerB,maxtime){
    let AHandle = await data.get(playerA);
    let BHandle = await data.get(playerB);
    let playerASub = await data.get(`SUB${AHandle}`);
    let playerBSub = await data.get(`SUB${BHandle}`);
    if(playerASub==undefined) playerASub = [];
    if(playerBSub==undefined) playerBSub = [];
    let tmp = await getProblem(['']);
    let result = tmp.result;
    let {problems} = result;
    let r;
    let arr1 = [], arr2 = [];
    let AData = await getUser(AHandle);
    let BData = await getUser(BHandle);
    let min_rtg = Math.min(800,AData.result[0].rating-500), max_rtg = Math.max(1200,AData.result[0].rating+500);
    for(let i = 0;i < 5;i++){
        r = Math.round(Math.random()*(problems.length-1))
        let count = 0;
        while(problems[r].contestId < 300 ||problems[r].tags.length==0||problems[r].tags.includes('*special')||playerASub.includes(problems[r].contestId+problems[r].index) || problems[r].rating < parseInt(parseInt(min_rtg)+parseInt(i*(max_rtg-min_rtg)/5))|| problems[r].rating > parseInt(parseInt(min_rtg)+parseInt((i+1)*(max_rtg-min_rtg)/5)) ||　problems[r].rating == null){
            count++;
            r = Math.round(Math.random()*(problems.length-1));
            if(count > 30000){
                msg.channel.send(`發生錯誤，找不到適合${playerA}的題目，可能出現難度不正確的題目`);
                break;
            }
        }
        arr1.push([parseInt(r),100*(i+1)]);
    }
    min_rtg = Math.min(800,BData.result[0].rating-500), max_rtg = Math.max(1200,BData.result[0].rating+500);
    for(let i = 0;i < 5;i++){
        r = Math.round(Math.random()*(problems.length-1))
        let count = 0;
        while(problems[r].contestId < 300 ||problems[r].tags.length==0||problems[r].tags.includes('*special')|| playerBSub.includes(problems[r].contestId+problems[r].index) || problems[r].rating < parseInt(parseInt(min_rtg)+parseInt(i*(max_rtg-min_rtg)/5))|| problems[r].rating > parseInt(parseInt(min_rtg)+parseInt((i+1)*(max_rtg-min_rtg)/5)) ||　problems[r].rating == null){
            count++;
            r = Math.round(Math.random()*(problems.length-1));
            if(count > 30000){
                msg.channel.send(`發生錯誤，找不到適合${playerB}的題目，可能出現難度不正確的題目`);
                break;
            }
        }
        arr2.push([parseInt(r),100*(i+1)]);
    }
    data.save(`P${playerA}`,problems);
    data.save(`ARR${playerA}`, arr1);
    data.save(`ARR${playerB}`, arr2);
    data.save(`WA${playerA}`,[]);
    data.save(`WA${playerB}`,[]);
    data.save(`HP${playerA}`,1000);
    data.save(`HP${playerB}`,1000);
    data.save(`MAXT${playerA}`,maxtime);
    await new Promise(r => setTimeout(r,2000));
    checkStatus2(msg,playerA,playerB,true);
    let timeStart = setInterval(timer,1000,playerA);
    let game = setInterval(checkStatus2,5000,msg,playerA,playerB,false);
    data.save(`T${playerA}`,0);
    let nowTime = await data.get(`T${playerA}`);
    let AHP = await data.get(`HP${playerA}`);
    let BHP = await data.get(`HP${playerB}`);
    let checkarr = await data.get(`ARR${playerA}`);
    while(nowTime<maxtime&&AHP>0&&BHP>0&&checkarr!=undefined){
        await new Promise(r => setTimeout(r,2000));
        nowTime = await data.get(`T${playerA}`);
        AHP = await data.get(`HP${playerA}`);
        BHP = await data.get(`HP${playerB}`);
        checkarr = await data.get(`ARR${playerA}`);
    }
    AHP = await data.get(`HP${playerA}`);
    BHP = await data.get(`HP${playerB}`);
    if(nowTime>=maxtime){
        let embed = new Discord.MessageEmbed()
            .setColor('#FF0000')
            .setTitle('時間到');
        msg.channel.send(embed);
    }else if(AHP>0&&BHP>0){
        let embed = new Discord.MessageEmbed()
            .setColor('#FF0000')
            .setTitle('強制結束');
        msg.channel.send(embed);
    }else if(AHP==0){
        let embed = new Discord.MessageEmbed()
            .setColor('#FF0000')
            .setTitle(`${AHandle}的血量已歸零`);
        msg.channel.send(embed);
    }else if(BHP==0){
        let embed = new Discord.MessageEmbed()
            .setColor('#FF0000')
            .setTitle(`${BHandle}的血量已歸零`);
        msg.channel.send(embed);
    }
    clearInterval(game);
    clearInterval(timeStart);
    
    let embed = new Discord.MessageEmbed()
        .setTitle('決鬥結果')
        .addField(`${AHandle}生命值`,AHP)
        .addField(`${BHandle}生命值`,BHP)
        .setColor('#FF5500');
    if(AHP > BHP) embed.addField('贏家',`${playerA}勝利`);
    else if(AHP < BHP) embed.addField('贏家',`${playerB}勝利`);
    else embed.addField('平手','無人勝利');
    msg.channel.send('',embed);
    data.save(`WA${playerA}`,[]);
    data.save(`WA${playerB}`,[]);
    data.del(`B${playerB}`);
    data.del(`B${playerA}`);
    data.del(`ARR${playerA}`);
    data.del(`ARR${playerB}`);
}

const checkStatus2 = async function(msg,playerA,playerB,first){
    let maxtime = await data.get(`MAXT${playerA}`);
    let problems = await data.get(`P${playerA}`);
    let arr1 = await data.get(`ARR${playerA}`);
    let arr2 = await data.get(`ARR${playerB}`);
    if(arr1==undefined || arr2 == undefined) return;
    let AWA = await data.get(`WA${playerA}`);
    let BWA = await data.get(`WA${playerB}`);
    let AHandle = await data.get(playerA);
    let BHandle = await data.get(playerB);
    let ASubmissions = await getUserSubmission(AHandle,5);
    let AResult = await ASubmissions.result;
    let BSubmissions = await getUserSubmission(BHandle,5);
    let BResult = await BSubmissions.result;
    if(AResult==undefined||BResult==undefined){
        console.log('發生問題');
        return;
    }
    let ok = false;
    for(let i = 0;i < AResult.length;i++){
        for(let j = 0;j < arr1.length;j++){
            if(AResult[i].problem.contestId==problems[arr1[j][0]].contestId&&AResult[i].problem.index==problems[arr1[j][0]].index){
                if(AResult[i].verdict === 'OK'){
                    ok = true;
                    msg.channel.send(`${playerA}已解出${problems[arr1[j][0]].name}，並對${playerB}造成${arr1[j][1]}點傷害`);
                    let BHP = await data.get(`HP${playerB}`);
                    data.save(`HP${playerB}`,Math.max(0,BHP-arr1[j][1]));
                    let r = Math.round((Math.random()*(problems.length-1)));
                    while(problems[r].contestId < 300 ||problems[r].tags.length==0||problems[r].tags.includes('*special')||usersub.includes(problems[r].contestId+problems[r].index) || problems[r].rating < problems[arr1[j][0]].rating || problems[r].rating > problems[arr1[j][0]].rating || problems[r].rating == null){
                        count++;
                        r = Math.round((Math.random()*(problems.length-1)));
                        if(count > 30000){
                            msg.reply('找不到同樣難度的題目');
                            break;
                        }
                    }
                    arr1[j][0] = r; 
                }else if(!AWA.includes(AResult[i].id)&&AResult[i].verdict!=='TESTING'){
                    msg.channel.send(`${playerA}解了${problems[arr1[j][0]].name}，但是答案錯了，讓我們一起嘲笑他吧`);
                    AWA.push(AResult[i].id);
                    data.save(`WA${playerA}`,AWA);
                }
            }
        }
    }
    for(let i = 0;i < BResult.length;i++){
        for(let j = 0;j < arr2.length;j++){
            if(BResult[i].problem.contestId==problems[arr2[j][0]].contestId&&BResult[i].problem.index==problems[arr2[j][0]].index){
                if(BResult[i].verdict === 'OK'){
                    ok = true;
                    msg.channel.send(`${playerB}已解出${problems[arr2[j][0]].name}，並對${playerA}造成${arr2[j][1]}點傷害`);
                    let AHP = await data.get(`HP${playerA}`);
                    data.save(`HP${playerA}`,Math.max(0,AHP-arr2[j][1]));
                    let r = Math.round((Math.random()*(problems.length-1)));
                    while(problems[r].contestId < 300 ||problems[r].tags.length==0||problems[r].tags.includes('*special')||usersub.includes(problems[r].contestId+problems[r].index) || problems[r].rating < problems[arr2[j][0]].rating || problems[r].rating > problems[arr2[j][0]].rating || problems[r].rating == null){
                        count++;
                        r = Math.round((Math.random()*(problems.length-1)));
                        if(count > 30000){
                            msg.reply('找不到同樣難度的題目');
                            break;
                        }
                    }
                    arr2[j][0] = r; 
                }else if(!BWA.includes(BResult[i].id)&&BResult[i].verdict!=='TESTING'){
                    msg.channel.send(`${playerB}解了${problems[arr2[j][0]].name}，但是答案錯了，讓我們一起嘲笑他吧`);
                    BWA.push(BResult[i].id);
                    data.save(`WA${playerB}`,BWA);
                }
            }
        }
    }
    data.save(`ARR${playerA}`,arr1);
    data.save(`ARR${playerB}`,arr2);
    if(ok||first){
        let AHP = await data.get(`HP${playerA}`);
        let BHP = await data.get(`HP${playerB}`);
        

        let embed = new Discord.MessageEmbed()
            .setTitle(`決鬥進行狀況 \n${AHandle}目前的生命值${AHP}\t\t  vs  \t\t${BHP}目前的生命值${BHandle}`)
            .setColor('#0055FF');
        let str = '';   
        for(let i = 0;i < arr1.length;i++){
            str += `難度:${problems[arr1[i][0]].rating}\t攻擊力:${arr1[i][1]}\t\t[${problems[arr1[i][0]].name}](http://codeforces.com/contest/${problems[arr1[i][0]].contestId}/problem/${problems[arr1[i][0]].index})`;
            if(i!=arr1.length-1) str += '\n';
        }
        embed.addField(`${AHandle}抽到的難度、攻擊力、題目`,str,true);
         str = '';   
        for(let i = 0;i < arr2.length;i++){
            str += `難度:${problems[arr2[i][0]].rating}\t攻擊力:${arr2[i][1]}\t\t[${problems[arr2[i][0]].name}](http://codeforces.com/contest/${problems[arr2[i][0]].contestId}/problem/${problems[arr2[i][0]].index})`;
            if(i!=arr2.length-1) str += '\n';
        }
        embed.addField(`${BHandle}抽到的難度、攻擊力、題目`,str,true);
        let nowTime = await data.get(`T${playerA}`);
        let hour = Math.floor((maxtime-nowTime)/3600);
        let minute = Math.floor((maxtime-nowTime)%3600/60);
        let second = (maxtime-nowTime)%60;
        str = `${hour}小時${minute}分鐘${second}秒`;
        embed.addField('剩餘時間',str);
        msg.channel.send('',embed);
    }
} 

const BattleStart = async function(msg,playerA,playerB,min_rtg,max_rtg,maxtime){
    let AHandle = await data.get(playerA);
    let BHandle = await data.get(playerB);
    let playerASub = await data.get(`SUB${AHandle}`);
    let playerBSub = await data.get(`SUB${BHandle}`);
    if(playerASub==undefined) playerASub = [];
    if(playerBSub==undefined) playerBSub = [];
    let tmp = await getProblem(['']);
    let result = tmp.result;
    let {problems} = result;
    let r;
    let arr = [];
    if(parseInt(min_rtg)+400 > parseInt(max_rtg)){
        max_rtg = parseInt(min_rtg)+400;
        msg.reply(`題目難度間隔太小已將難度上界調為${max_rtg}`);
    } 
    for(let i = 0;i < 5;i++){
        r = Math.round(Math.random()*(problems.length-1))
        let count = 0;
        while(problems[r].contestId < 300 ||problems[r].tags.length==0||problems[r].tags.includes('*special')||playerASub.includes(problems[r].contestId+problems[r].index) || playerBSub.includes(problems[r].contestId+problems[r].index) || problems[r].rating < parseInt(parseInt(min_rtg)+parseInt(i*(max_rtg-min_rtg)/5))|| problems[r].rating > parseInt(parseInt(min_rtg)+parseInt((i+1)*(max_rtg-min_rtg)/5)) ||　problems[r].rating == null){
            count++;
            r = Math.round(Math.random()*(problems.length-1));
            if(count > 30000){
                msg.channel.send('發生錯誤，找不到適合你們的題目，可能出現難度不正確的題目');
                break;
            }
        }
        arr.push([parseInt(r),100*(i+1)]);
    }
    data.save(`P${playerA}`,problems);
    data.save(`ARR${playerA}`, arr);
    data.save(`WA${playerA}`,[]);
    data.save(`WA${playerB}`,[]);
    data.save(`SCORE${playerA}`,0);
    data.save(`SCORE${playerB}`,0);
    data.save(`MAXT${playerA}`,maxtime);
    await new Promise(r => setTimeout(r,2000));
    checkStatus(msg,playerA,playerB,true);
    let timeStart = setInterval(timer,1000,playerA);
    let game = setInterval(checkStatus,5000,msg,playerA,playerB,false);
    let checkarr = await data.get(`ARR${playerA}`);
    data.save(`T${playerA}`,0);
    let nowTime = await data.get(`T${playerA}`);
    while((checkarr!=undefined||checkarr.length!=0)&&nowTime<maxtime){
        await new Promise(r => setTimeout(r,2000));
        nowTime = await data.get(`T${playerA}`);
        checkarr = await data.get(`ARR${playerA}`);
        if(checkarr==undefined) break;
    }
    console.log('Stopped');
    if(nowTime>=maxtime){
        let embed = new Discord.MessageEmbed()
            .setColor('#FF0000')
            .setTitle('時間到');
        msg.channel.send(embed);
    }else{
        let embed = new Discord.MessageEmbed()
            .setColor('#FF0000')
            .setTitle('強制結束');
        msg.channel.send(embed);
    }
    clearInterval(game);
    clearInterval(timeStart);
    let AScore = await data.get(`SCORE${playerA}`);
    let BScore = await data.get(`SCORE${playerB}`);
    let winner = (AScore > BScore ? playerA : playerB);
    let tie = (AScore == BScore);
    let embed = new Discord.MessageEmbed()
        .setTitle('決鬥結果')
        .addField(`${AHandle}分數`,AScore)
        .addField(`${BHandle}分數`,BScore)
        .setColor('#FF5500');
    if(AScore > BScore) embed.addField('贏家',`${playerA}勝利`);
    else if(AScore < BScore) embed.addField('贏家',`${playerB}勝利`);
    else embed.addField('平手','無人勝利');
    msg.channel.send('',embed);
    data.save(`WA${playerA}`,[]);
    data.save(`WA${playerB}`,[]);
    data.del(`B${playerB}`);
    data.del(`B${playerA}`);
    data.del(`ARR${playerA}`);
    data.del(`ARR${playerB}`);
}

const checkStatus = async function(msg,playerA,playerB,first){
    let maxtime = await data.get(`MAXT${playerA}`);
    let problems = await data.get(`P${playerA}`);
    let arr = await data.get(`ARR${playerA}`);
    let AWA = await data.get(`WA${playerA}`);
    let BWA = await data.get(`WA${playerB}`);
    if(arr==undefined) return;
    let AHandle = await data.get(playerA);
    let BHandle = await data.get(playerB);
    let ASubmissions = await getUserSubmission(AHandle,5);
    let AResult = await ASubmissions.result;
    let BSubmissions = await getUserSubmission(BHandle,5);
    let BResult = await BSubmissions.result;
    if(AResult==undefined||BResult==undefined){
        console.log('發生問題');
        return;
    }
    let ok = false;
    for(let i = 0;i < AResult.length;i++){
        for(let j = 0;j < arr.length;j++){
            if(AResult[i].problem.contestId==problems[arr[j][0]].contestId&&AResult[i].problem.index==problems[arr[j][0]].index){
                if(AResult[i].verdict === 'OK'){
                    ok = true;
                    msg.channel.send(`${playerA}已解出${problems[arr[j][0]].name}，並獲得${arr[j][1]}分`);
                    let AScore = await data.get(`SCORE${playerA}`);
                    data.save(`SCORE${playerA}`,AScore+arr[j][1]);
                    arr.splice(j,1);
                    j = -1;   
                }else if(!AWA.includes(AResult[i].id)&&AResult[i].verdict!=='TESTING'){
                    msg.channel.send(`${playerA}解了${problems[arr[j][0]].name}，但是答案錯了，讓我們一起嘲笑他吧`);
                    AWA.push(AResult[i].id);
                    data.save(`WA${playerA}`,AWA);
                }
            }
        }
    }
    for(let i = 0;i < BResult.length;i++){
        for(let j = 0;j < arr.length;j++){
            if(BResult[i].problem.contestId==problems[arr[j][0]].contestId&&BResult[i].problem.index==problems[arr[j][0]].index){
                if(BResult[i].verdict === 'OK'){
                    ok = true;
                    msg.channel.send(`${playerB}已解出${problems[arr[j][0]].name}，並獲得${arr[j][1]}分`);
                    let BScore = await data.get(`SCORE${playerB}`);
                    data.save(`SCORE${playerB}`,BScore+arr[j][1]);
                    arr.splice(j,1);
                    j = -1;    
                }else if(!BWA.includes(BResult[i].id)&&BResult[i].verdict!=='TESTING'){
                    msg.channel.send(`${playerB}解了${problems[arr[j][0]].name}，但是答案錯了，讓我們一起嘲笑他吧`);
                    BWA.push(BResult[i].id);
                    data.save(`WA${playerB}`,BWA);
                }
            }
        }
    }
    data.save(`ARR${playerA}`,arr);
    if(arr.length==0) return;
    if(ok||first){
        let AScore = await data.get(`SCORE${playerA}`);
        let BScore = await data.get(`SCORE${playerB}`);
        

        let embed = new Discord.MessageEmbed()
            .setTitle(`決鬥進行狀況 \n${AHandle}目前的分數${AScore}  vs  ${BScore}目前的分數${BHandle}`)
            .setColor('#0055FF');
        let str = '';   
        for(let i = 0;i < arr.length;i++){
            str += `[${problems[arr[i][0]].name}](http://codeforces.com/contest/${problems[arr[i][0]].contestId}/problem/${problems[arr[i][0]].index})`;
            if(i!=arr.length-1) str += '\n';
        }
        embed.addField('題目',str);
        str = '';
        for(let i = 0;i < arr.length;i++){
            str += `難度:${problems[arr[i][0]].rating}\t分數:${arr[i][1]}`;
            if(i!=arr.length-1) str += '\n';
        }
        embed.addField('難度及分數',str);
        let nowTime = await data.get(`T${playerA}`);
        let hour = Math.floor((maxtime-nowTime)/3600);
        let minute = Math.floor((maxtime-nowTime)%3600/60);
        let second = (maxtime-nowTime)%60;
        str = `${hour}小時${minute}分鐘${second}秒`;
        embed.addField('剩餘時間',str);
        msg.channel.send('',embed);
    }
}

const timer = async function(playerA){
    let tick = await data.get(`T${playerA}`);
    data.save(`T${playerA}`,tick+1);
}

const duel = {
    name: 'duel',
    usage: 'duel [user]',
    execute,
}
export default duel;