import Discord from 'discord.js';

const execute = async function(msg,args){
    let channel = msg.channel;
    msg.delete();
    if(args.length!=1&&args.length!=2){
        channel.send('指令用法錯誤，正確用法:!del (要刪除的訊息數量)').then(sentmsg=>{sentmsg.delete({timeout:1000})});
    }
    if(msg.author.id!=324889669265391618){
        channel.send('此指令只有路過的一隻山姆可以使用').then(sentmsg=>{sentmsg.delete({timeout:1000})});
        return;
    }
    const fetched = await channel.messages.fetch({limit: args[0]});
    channel.bulkDelete(fetched);
    channel.send(new Discord.MessageEmbed().setColor('#FF0000').setTitle(`已刪除${args[0]}筆訊息`)).then(sentmsg=>{sentmsg.delete({timeout:1000})});
}

const del = {
    name: 'del',
    usage: '!del [message amount]',
    execute,
}
export default del;