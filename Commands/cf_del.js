import Discord from 'discord.js';
import data from './Database/data.js';

const execute = async function(msg,args){
    if(args.length!=0){
        msg.reply('指令用法錯誤。正確用法: %cf_del');
        return;
    }
    if(data.get(`<@${msg.author.id}>`) == null){
        msg.reply('你尚未註冊CF');
        return;
    }else{
        data.del(`<@${msg.author.id}>`);
        msg.reply('已經刪除與你的CF帳號的連接');
    }
}

const cf_del = {
    name: 'cf_del',
    usage: 'cf_del [handle]',
    execute,
}
export default cf_del;