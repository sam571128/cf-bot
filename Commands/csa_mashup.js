import Discord from 'discord.js';
import data from './Database/data.js';
import puppeteer from 'puppeteer';

let tasks = []

const grabProblems = async function(){
    const browser = await puppeteer.launch();
    const page = await browser.newPage();

    await page.goto('https://csacademy.com/contest/archive/');

    await new Promise((resolve, reject) => setTimeout(resolve, 3000));
    
    const hrefs = await page.$$eval('a', as => as.map(a => a.href));

    tasks = []
    for(let i = 0; i < hrefs.length; i++){
        if(hrefs[i].includes("task/")){
                tasks.push(hrefs[i].substr(43, hrefs[i].length - 43 - 1));
        }
    }
    browser.close();
}


const execute = async function(msg,args){
    let channel = msg.channel;
    let target = msg.author;

    channel.send(`<@${target.id}> 正在抓取 CSAcademy 的題目...`).then(sentmsg=>{sentmsg.delete({timeout:3000})});;
    
    await grabProblems();

    let problems = []
    while(problems.length < 5){
        let r = Math.floor(Math.random() * tasks.length);
        if(!problems.includes(tasks[r])){
            problems.push(tasks[r])
        }
    }
    let embed = new Discord.MessageEmbed()
            .setTitle('CSAcademy 題單')
            .setColor('#FFFF00');
        let str = '';
        for(let i = 0;i < problems.length;i++){
            str += `[${problems[i]}](http://csacademy.com/contest/archive/task/${problems[i]})/`;
            if(i!=problems.length-1) str += '\n';
        }
        embed.addField('題目',str);
        const embedFooter = `使用者: ${msg.author.tag}`;
        embed.setFooter(embedFooter, `${msg.author.displayAvatarURL({
            size: 4096,
            dynamic: true
        })}`);
        msg.channel.send('',embed);
}


const csa_mashup = {
    name: 'csa_mashup',
    usage: '%csa_mashup [count]',
    execute
}
export default csa_mashup;