import Discord from 'discord.js';
import data from './Database/data.js';

const execute = async function(msg,args){
    let channel = msg.channel;
    let target = msg.mentions.users.first();

    if(!target) target = msg.author;

    let list = await data.get(`CHEFLIST`);
    
    if(!list) list = [];

    if(!list.includes(`<@${target.id}>`)) list.push(`<@${target.id}>`);

    let cnt = await data.get(`CHEF<@${target.id}>`);
    if(!cnt){
        cnt = 0;
    }

    cnt++;

    msg.channel.send(`<@${target.id}> 好電 被廚了 ${cnt} 次了！`);

    data.save(`CHEF<@${target.id}>`,cnt);


    data.save(`CHEFLIST`,list);
}


const chef = {
    name: 'chef',
    usage: '%chef [someone]',
    execute
}
export default chef;