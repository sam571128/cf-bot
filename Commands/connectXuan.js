import Discord from 'discord.js';
import data from './Database/data.js';
import {getXuan} from './cf-api/api.js';

const execute = async function(msg,args){
    console.log('Connecting');
    let users = await data.get('USER');
    if(users == undefined){
        data.save('USER',[]);
        users = [];
    }
    let tmp = await getXuan();
    let result = tmp.result;
    for(let i = 0;i < result.length;i++){
        console.log(result[i].DiscoidUID,result[i].CFHandle);
        if(!users.includes(result[i].CFHandle)) users.push(result[i].CFHandle);
        data.save(`<@${result[i].DiscoidUID}>`,result[i].CFHandle);
    }
    data.save('USER',users);
    msg.reply('連接成功');
}

const connectXuan = {
    name: 'connectxuan',
    usage: '%connectxuan',
    execute,
}
export default connectXuan;